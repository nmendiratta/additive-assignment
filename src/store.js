import {  createStore, compose, applyMiddleware } from 'redux';
import reducer from './reducers/index';
import thunk from 'redux-thunk';

// import LocalStorage from './services/LocalStorage';

// const localStore = new LocalStorage();

// noinspection JSUnresolvedVariable
/**
 * Future proofing
 * `window.devToolsExtension` is deprecated in favor of
 * `window.__REDUX_DEVTOOLS_EXTENSION__`, and will be removed in next version
 * of Redux DevTools
 */
// eslint-disable-next-line no-underscore-dangle
// const reduxDevTool = window.__REDUX_DEVTOOLS_EXTENSION__ || window.devToolsExtension;

// const persistedState = localStore.loadState('state');

// f is called identity function
const store = createStore(
  reducer,
  applyMiddleware(thunk)
  // persistedState,
  // compose(
  //   applyMiddleware(),
  //   typeof window === "object" && typeof reduxDevTool !== "undefined" ? reduxDevTool() : f => f
  // )
);

export default store;
