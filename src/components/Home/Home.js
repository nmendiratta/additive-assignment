
import React, { Component } from 'react';

import Header from '../Header/Header';
import SearchBar from '../SearchBar/SearchBar';

class Home extends Component {


  render () {

    return (
      <div className="section clearfix">
        <div className="jumbotron-heading">
          <Header text="Employee Directory" back={false} name="Find employ hierarchy"/>
          <SearchBar/>
        </div>
      </div>
    );
  }
}


export default Home;