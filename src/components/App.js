import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from '../store';
import Home from './Home/Home';
import EmployeePage from './Employee/Page';

class App extends Component {

  constructor (props) {
    super(props);
  }

  render () {
    return (
      <BrowserRouter>
        <Provider store={store}>
          <div className="container">
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/overview/:name" component={EmployeePage}/>
            </Switch>
          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
