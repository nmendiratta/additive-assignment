import React, { Component } from 'react';


class Avatar extends Component {

  render () {
    return (
      <div className={`avatar ${this.props.className}`}>
        <div className="initial">
          {this.props.name[0]}
        </div>
      </div>
    );
  }
}

export default Avatar;