
import React from 'react';

const List = (props) => (
  <div className='list-view'>
    {
      props.header && props.header.title ?
        <div className='list-view__head'>
          <div className='title-block'>
            {
              props.header.tag &&
              <span className='tag'>{props.header.tag}</span>
            }
            <span className='title'>{props.header.title}</span>
            {
              props.header.subtitle &&
              <span className="subtitle">{props.header.subtitle}</span>
            }
          </div>
        </div> : <div/>
    }
    <ul className='list-view__body'>
      {props.body ? props.body.map(item => item) : ''}
      {
        props.children && props.children.length &&
        props.children.map(node => <li key={node.key}>{node}</li>)
      }
    </ul>
    {
      props.footer ?
        <ul className='list-view__footer'>
          <li> {props.footer.text} </li>
        </ul>
        :
        <div/>
    }
  </div>
);

export default List;
