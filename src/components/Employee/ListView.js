
import React from 'react';
import List from './List/List';

const ListView = (props) => (
  <List {...props} />
);

export default ListView;
