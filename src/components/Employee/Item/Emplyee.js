import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from '../../Avatar/Avatar';

const colorList = [
  'orange',
  'ciyan',
  'grey',
  'blue',
  'red'
];

const EmployeeItem = (props) => (
  <li className='list-view__body__item' key={`${props.index}`}>
    <Link to={`/overview/${props.employee.name}`}>
      <div className='content data'>
        <Avatar name={props.employee.name} className={colorList[Math.floor(Math.random() * 5)]}/>
        <div className="sub-list">
          <span className="name"> {props.employee.name} </span>
          <span className="profile muted"> ({props.employee.profile}) </span>
        </div>
      </div>
    </Link>
  </li>
);

export default EmployeeItem;
