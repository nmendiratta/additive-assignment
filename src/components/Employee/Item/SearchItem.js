
import React from 'react';
import Avatar from '../../Avatar/Avatar';
const colorList = [
  "orange",
  "ciyan",
  "grey",
  "blue",
  "red"
];

const EmployeeItem = (props) => (
  <li className='list-view__body__item' key={`${props.index}`} onClick={() => props.onSelect({target: {value : props.employee}}, true)}>
    <div className='content data'>
      <Avatar name={props.employee} className={colorList[Math.floor(Math.random() * 5)]}/>

      <div>
        <p> {props.employee} </p>
      </div>
    </div>
  </li>
);

export default EmployeeItem;
