
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getEmployeeById } from '../../actions';
import Loader from '../Loader/Loader';
import EmployeeItem from './Item/Emplyee';
import EmployeeService from '../../Services/employee';
import Header from '../Header/Header';

class EmployeePage extends Component {

  state = {
    profile: '',
    subordinates: [],
  };

  componentDidMount = () => {
    this.props.getEmployeeById(this.props.match.params.name);
  };

  componentWillReceiveProps (newProps) {
    if (newProps.subOrdinateObjectList) {
      this.setSubordinates(newProps.subOrdinateObjectList);
    }
    if (newProps.employeeDetails && newProps.employeeDetails.length) {
      this.setEmployeeDetails(newProps.employeeDetails);
    }
    if (newProps && this.props) {
      if (newProps.match.params.name !== this.props.match.params.name) {
        this.props.getEmployeeById(newProps.match.params.name);
      }
    }
  }

  setEmployeeDetails = (employeeDetails) => {
    this.setState({
      profile: employeeDetails[0],
    });
  };

  setSubordinates = (subOrdinateList) => {
    this.setState({
      subordinates: EmployeeService.removeDuplicatesObjects(subOrdinateList)
    }, () => {
    });
  };

  render () {
    const {isLoading, match} = this.props;
    const {subordinates, profile} = this.state;

    return (
      <div className="section clearfix">
        <Header text="Employee Detail" {...this.props} back={true} name={match.params.name} profile={profile}/>
        <div className="heading">
          {
            isLoading && <Loader/>
          }
          <div className="page">
            {
              !isLoading && subordinates.length ? <div className="lead list-heading">List of sub-ordinates</div> : <div />
            }
            <div className="card">
              {
                !isLoading && subordinates.length ? subordinates.map((item, index) => <EmployeeItem key={index} index={index} employee={item}/>) :
                  <div className="no-data-found"> No subordinates found !</div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    employeeDetails: state.employeeDetails,
    isLoading: state.inProgress,
    subOrdinateObjectList: state.subOrdinateObjectList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({getEmployeeById}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeePage);