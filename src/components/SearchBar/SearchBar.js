import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import EmployeeItem from '../Employee/Item/SearchItem';
import { getEmployeeList } from '../../actions';

import EmployeeService from '../../Services/employee';

import Loader from '../Loader/Loader';

class SearchBar extends Component {

  state = {
    searchKey: '',
    employeeList: [],
    error: false,
    selected: false,
  };

  componentDidMount () {
    this.props.getEmployeeList();
  }

  componentWillReceiveProps (newProps) {
    if (newProps.employeeList && newProps.employeeList.length) {
      this.setEmployeeList(newProps.employeeList);
    }
  }

  setEmployeeList = (employeeList, selected = false) => {
    this.setState({employeeList, selected});
  };

  onKeyPressHandler = (e) => {
    if (this.state.error) {
      this.setState({
        error: false
      })
    }
    if (e.key === 'Enter') {
      this.searchHandler(e, false);
    }
  };

  searchHandler = (event, selected) => {
    let searchKey = event.target.value;
    this.setState({searchKey: searchKey}, () => {
      let data = EmployeeService.findByName(this.props.employeeList, this.state.searchKey);
      this.setEmployeeList(data, selected);
    });
  };

  onClickHandler = () => {
    if (this.state.searchKey) {
      this.props.history.push(`/overview/${this.state.searchKey}`);
    } else {
      this.setState({
        error: true
      });
    }
  };

  clearSearch = () => {
    this.searchHandler({target: {value: ''}}, false);
    this.props.getEmployeeList();
  };

  render () {
    const {employeeList} = this.state;
    const {isLoading} = this.props;

    return (
      <div>
        <div className={this.state.error ? "err" : "err hide-opacity"}>Please enter a valid name!</div>
        <div className="section search-box">
          <div className="bar bar-standard">
            <div className="search-input">
              <input type="search" placeholder="Search with employee name..." className={this.state.selected ? 'disabled' : ''} value={this.state.searchKey}
                     disabled={this.state.selected} onKeyPress={this.onKeyPressHandler} onChange={this.searchHandler}/>
              {
                this.state.selected && <span className="clear-search" onClick={this.clearSearch}>x</span>
              }
              {
                !this.state.selected && (
                  <div className="search-list-content">
                    {
                      isLoading && <Loader/>
                    }
                    {
                      !isLoading && employeeList.length ? employeeList.map((item, index) => <EmployeeItem onSelect={this.searchHandler} key={index}
                                                                                                          index={index} employee={item}/>) :
                        <div className="no-data-found"> No records found !</div>
                    }
                  </div>
                )
              }
            </div>
            <div className="btn search-btn">
              <button onClick={this.onClickHandler}>Search</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    employeeList: state.employeeList,
    isLoading: state.inProgress,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({getEmployeeList}, dispatch);
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar));