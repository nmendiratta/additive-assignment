import React, { Component } from 'react';

class Heading extends Component {

  goBack = () => {
    this.props.history.goBack();
  };

  render () {
    return (
      <div className="section clearfix">
        <div className="header-section">
          {
            this.props.back ? <div>
              <button className="back-btn" onClick={() => this.goBack()}>← Back</button>
            </div> : <div />
          }
          <header className="bar bar-nav">
            <h1 className="title">{this.props.text}</h1>
            {
              this.props.name && <div className="lead text-muted name">{this.props.name}</div>
            }
            {
              this.props.profile && <div className="lead text-muted sub-tag">({this.props.profile})</div>
            }
          </header>
          <div />
        </div>
      </div>
    );
  }
}

export default Heading;