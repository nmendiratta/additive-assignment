import axios from 'axios';

export const ALL_EMPLOYEES = 'ALL_EMPLOYEES';
export const EMPLOYEE = 'EMPLOYEE';
export const SUB_ORDINATES = 'SUB_ORDINATES';
export const SUB_ORDINATES_OBJECT_LIST = 'SUB_ORDINATES_OBJECT_LIST';
export const RESET_SUB_ORDINATES = 'RESET_SUB_ORDINATES';
export const IN_PROGRESS = 'IN_PROGRESS';

const ENV = {
  apiHost: 'http://api.additivasia.io/api/v1/assignment',
};

const setEmployeeList = (data) => ({
  type: ALL_EMPLOYEES,
  payload: data
});

const setEmployeeDetails = (data) => ({
  type: EMPLOYEE,
  payload: data
});

export function inProgress (status) {
  return {type: IN_PROGRESS, payload: status};
}

export const getEmployeeList = () => (dispatch) => {

  const list = [
    'John Hartman',
    'Samad Pitt',
    'Amaya Knight',
    'Leanna Hogg',
    'Aila Hodgson',
    'Vincent Todd',
    'Faye Oneill',
    'Lynn Haigh',
    'Nylah Riddle'
  ];

  dispatch(setEmployeeList(list));
};

// export function setSubOrdinates (data) {
//   return {type: SUB_ORDINATES, payload: data};
// }

export function setSubOrdinatesObjectList (data) {
  return {type: SUB_ORDINATES_OBJECT_LIST, payload: data};
}

export function reSetSubOrdinates () {
  return {type: RESET_SUB_ORDINATES, payload: []};
}

export const getEmployeeById = (name) => (dispatch) => {

  dispatch(reSetSubOrdinates());
  dispatch(inProgress(true));
  axios({
    method: 'get',
    url: `${ENV.apiHost}/employees/${name}`,
  }).then(response => {

    if (response.data.length > 0 && response.data[1]) {
      getIndirectSubordinates(response.data[1]['direct-subordinates'], dispatch);
    }

    dispatch(setEmployeeDetails(response.data));

  }).catch(error => {
    dispatch(inProgress(false));
  }).then(() => { // Axios don't support `finally`. This is Axios's `finally` implementation
    dispatch(inProgress(false));
  });
};

const getIndirectSubordinates = (subordinatesToFetch, dispatch) => {
  let promise = [];

  subordinatesToFetch.map((item) => {
    promise.push(axios.get(`http://api.additivasia.io/api/v1/assignment/employees/${item}`));
  });

  Promise.all(promise).then(function (values) {
    let done = true;
    let newSubordinatesToFetchList = [];
    values.forEach((item) => {

      const tempObj = {
        name: item.config.url.split('/').pop(),
        profile: item.data[0]
      };

      // keeping subordinates array[objects, object] in store for showing on detail page
      dispatch(setSubOrdinatesObjectList(tempObj));
      if (item.data && item.data[1]) {
        let data = item.data[1]['direct-subordinates'];
        if (data && data.length) {
          done = false;
          newSubordinatesToFetchList = newSubordinatesToFetchList.concat(data);
        }
      }

    });
    if (!done) {
      getIndirectSubordinates(newSubordinatesToFetchList, dispatch);
    }

  });
};