
import { combineReducers } from 'redux';
import { ALL_EMPLOYEES, EMPLOYEE, IN_PROGRESS, RESET_SUB_ORDINATES, SUB_ORDINATES, SUB_ORDINATES_OBJECT_LIST } from '../actions';

const employeeList = (state = {}, action) => {
  if (action.type === ALL_EMPLOYEES) {
    return action.payload;
  }
  return state;
};

const employeeDetails = (state = {}, action) => {
  if (action.type === EMPLOYEE) {
    return action.payload;
  }
  return state;
};

export const inProgress = (state = false, action) => {
  if (action.type === IN_PROGRESS) {
    return action.payload;
  }
  return state;
};

export const subOrdinateObjectList = (state = [], action) => {
  if (action.type === RESET_SUB_ORDINATES) {
    return action.payload;
  } else if (action.type === SUB_ORDINATES_OBJECT_LIST) {
    return state.concat(action.payload);
  }
  return state;
};

export const reducers = combineReducers({
  employeeDetails,
  inProgress,
  employeeList,
  subOrdinateObjectList,
});

export default reducers;