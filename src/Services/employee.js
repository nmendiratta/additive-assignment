class EmployeeService {

  findByName = (employees, searchKey) => {
    return employees.filter(function (element) {
      return element.toLowerCase().indexOf(searchKey.toLowerCase()) > -1;
    });
  };

  removeDuplicatesObjects (arr) {
    let obj = {};
    let retArr = [];
    for (let i = 0; i < arr.length; i++) {
      obj[arr[i].name] = arr[i];
    }
    for (let key in obj) {
      retArr.push(obj[key]);
    }
    return retArr;
  }

}

export default new EmployeeService();