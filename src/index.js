import 'babel-polyfill';
import React from 'react';
import ReactDom from 'react-dom';


import TheComponent from './components/App';
import './styles/main.scss';

ReactDom.render(<TheComponent />, document.getElementById('app'));